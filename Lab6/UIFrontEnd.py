'''
@file UIFrontEnd.py

@brief This script acts as the Spyder user interface for lab 6

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import serial
import matplotlib.pyplot as plt
import time

# Create a serial port for nucleo communication
ser1 = serial.Serial(port='COM3',baudrate=115273,timeout=0.5)

# run
for n in range(4):
    
    # Create Time and Position lists
    Time = []
    Velocity = []
    
    # Ask for user input
    inv = input('Please enter a positive number for gain Kp: ')
    
    # Check if user input is valid
    try:
        test = float(inv)
        print('Thank you, data is now being recorded.')
        # Send user input to nucleo
        ser1.flushInput()
        ser1.write(str(inv).encode('ascii'))
        # Wait for data to be collected and sent
        time.sleep(5)
        
        # Organize data into the proper data structures
        while ser1.in_waiting > 0:
            line_string = ser1.readline().decode('ascii')
            line_list = line_string.strip().split(',')
                                                  
            Time.append(float(line_list[0]))
            Velocity.append(float(line_list[1]))
        
        # Plot data
        plt.plot(Time,Velocity)
        plt.plot([0,0.01,5],[0,1000,1000])
        plt.xlabel('Time [s]')
        plt.ylabel('Motor Speed [RPM]')
        plt.show()  
    except:
        print('Invalid input, please try again')
ser1.close()
    
   
    

    