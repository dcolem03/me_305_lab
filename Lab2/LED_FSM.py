'''
@file LED_FSM.py

This file serves as an example of a finite state machine implemented onto the
hardware of a nucleo board. This file contains code to create a FSM that controls
the brightness of either a physical LED on the board, or a virtual LED.

The physical LED follows a sawtooth pattern, increasing in brightness before falling
off while the virtual LED has an on and an off state depending on the "supplied power".

'''

from random import choice
import utime
import pyb

class TaskLED:
    '''
    @brief      A finite state machine to control an LED.
    @details    This class implements a finite state machine to control the
                operation of an LED.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT = 0    
    
    ## Constant defining State 1
    S1_RUN  = 1    
    
    def __init__(self, Period, Step, LED):
        '''
        @brief              Creates a TaskLED object.
        @param Period       An integer value representing a period multiplier to lengthen the time between pulses
        @param Step         A float that represents the step size in seconds
        @param LED          An object that can either be a real or virtual LED 
        '''

        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The value used as a period 
        self.Period = Period
        
        ## The step size to be used in the counter in ms
        self.Step = int(Step*1000)
        
        ## The LED object that can be virtual or real
        self.LED = LED

        ## The timestamp for when the task should run next
        self.next_time = utime.ticks_add(utime.ticks_ms(),self.Step)
        
        ## Integer that tracks the brightness of the LED
        self.brightness = 0
        
        ## Integer that counts the number of runs
        self.runs = 0
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Redefines the current time after every loop and keeps a constant value
        ## unreleated to which state the object is in
        self.curr_time = utime.ticks_ms()
        
        if self.curr_time > self.next_time:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.state = self.S1_RUN
                
            elif(self.state == self.S1_RUN):
                # Run State 1 Code
                
                # Reset the brightness if it passes 100% to match sawtooth pattern
                if (self.brightness>100):
                    self.brightness = 0
                    
                # Check if the LED is real to determine task to complete
                if self.LED.isReal():
                    self.LED.setBrightness(self.brightness)
                else:
                    self.LED.getBrightness(self.brightness)
                    
                # Increment the brightness
                self.brightness += 100*self.Step/(self.Period*1000)
                
            else:
                # Invalid state code (error handling)
                print ('error')
                pass
                
            # Increment runs
            self.runs +=1
                
            # Specifying the next time the task will run
            self.next_time += self.Step
                    
            
class RealLED:
    '''
    @brief      A real LED object
    @details    Create a real LED object that has control over the brightness of
                the built in LED on the nucleo board
    '''
    
    def __init__(self):
        '''
        @brief    Creates real LED
        '''
        self.pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
        self.tim2 = pyb.Timer(2, freq = 20000)
        self.t2ch1 = self.tim2.channel(1, pyb.Timer.PWM, pin=self.pinA5)
        
        print('Real LED Created')

    def isReal(self):
        '''
        @brief      Gets the LED reality.
        @details    This function will always return true
        @return     A boolean representing the reality of the LED.
        '''
        return True
    
    def setBrightness(self, PWM):
        '''
        @brief      Gets the LED .
        @details    This function will set the brighness of the LED on a nucleo
                    based on the input PWM parameter
        @param PWM  A value from 0-100 that sets the PWM brightness of the LED
        '''
        self.PWM = PWM
        
        self.t2ch1.pulse_width_percent(self.PWM)
        
class VirtualLED:
    '''
    @brief      A virtual LED object  
    @details    Create a virtual LED object that can print its state
    '''
    
    def __init__(self):
        '''
        @brief       Creates virtual LED
        '''
        print('Virtual LED created')

    def isReal(self):
        '''
        @brief      Gets the LED reality.
        @details    This function will always return false
        @return     A boolean representing the reality of the LED.
        '''
        return False

    def getBrightness(self, PWM):
        '''
        @brief      Gets the LED state.
        @details    This function gets the "brightness" of the LED by printing 
                    when the LED is on and when it is off. Currently the threshold
                    is set at 50% PWM.
        @param PWM  A value from 0-100 that sets the PWM "brightness" of the fake LED
        '''
        self.PWM = PWM
        
        if self.PWM<50:
            print('Virtual LED off')
        else:
            print('Virtual LED on')
