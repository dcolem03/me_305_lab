'''
@file Encoder_Task.py

@brief Encoder task as a finite state machine.

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import shares
import utime
from Encoder import Encoder

class TaskEncoder:
    '''
    @brief      Encoder task.
    
    @details    An object of this class updates the encoder object and checks if an 
                input is handed from the shares file. Once an input is recieved, 
                then the corresponding output is sent to the shares file.
    
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Update encoder
    S1_UPDATE           = 1

    def __init__(self, my_Encoder, taskNum, interval, dbg):
        '''
        Creates a cipher task object.
        @param my_Encoder An encoder object to be updating in the FSM
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The object controled by this task
        self.my_Encoder = my_Encoder
        
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The timestamp for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        if self.dbg:
            print('Created scaler task')

    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_UPDATE)
            
            elif(self.state == self.S1_UPDATE):
                self.printTrace()
                # Run State 1 Code
                self.my_Encoder.update()
                if shares.cmd == 122:
                    shares.resp = '\nEncoder Position Reset\n'
                    self.my_Encoder.set_Position(0)
                elif shares.cmd == 112:
                    shares.resp = '\nThe current postion is: ' + str(self.my_Encoder.get_Position()) + '\n'
                elif shares.cmd == 100:
                    shares.resp = '\nThe current delta is: ' + str(self.my_Encoder.get_Delta()) + '\n'
                else:
                    shares.resp = '\nError, invalid input\n'
                if shares.cmd:
                    shares.cmd = None
                                       
            else:
                # error handling
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
    


