'''
@file UI_Task.py

@brief User interface task as a finite state machine

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import shares
import utime
from pyb import UART

class TaskUI:
    '''
    @brief      User interface task.
    
    @details    An object of this class waits to recieve a user input and when
                it is recieved, it sends the input into the shares file. Once a
                input has been recieved and sent, the object waits to recieve a 
                response back from shares and prints it when it is recieved.
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2

    def __init__(self, taskNum, interval, dbg):
        '''
        Creates a user interface task object.
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                print('Please input one of the following commands: \n z : Zero the encoder position \n p : Print out the encoder position \n d : Print out the encoder delta')
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printTrace()
                # Run State 1 Code
                if self.ser.any():
                    shares.cmd = self.ser.readchar()
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
            
            elif(self.state == self.S2_WAIT_FOR_RESP):
                self.printTrace()
                # Run State 2 Code
                if shares.resp != None:
                    print(shares.resp)
                    shares.resp = None
                    print('Please input one of the following commands: \n z : Zero the encoder position \n p : Print out the encoder position \n d : Print out the encoder delta')
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)          
            else:
                # error handling
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
