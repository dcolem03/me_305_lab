'''
@file shares.py

@brief A file for all the inter-task variables

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

## The command character sent from the UI task to the Encoder task
cmd = None

## The response from the Encoder task sent to the UI task
resp = None