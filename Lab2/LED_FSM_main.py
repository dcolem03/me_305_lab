
'''
@file LED_FSM_main.py

This file creates two LED task objects, one real and one virtual, and runs them simultaneously
'''
from LED_FSM import TaskLED, RealLED, VirtualLED

# Create two LED objects, one real, one virtual to test simultaneously
LED1 = RealLED()
LED2 = VirtualLED()

# Set step size
Step_Size = 0.1

# Creating two task objects using the button and motor objects above
taska = TaskLED(10, Step_Size, LED1)
taskb = TaskLED(10, Step_Size, LED2)

# Run the tasks simultaneously
for N in range(100000):
    taska.run()
    taskb.run()