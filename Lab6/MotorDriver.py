'''
@file MotorDriver.py

@brief File containing a motor driver

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb

class MotorDriver:
    ''' 
    This class implements a motor driver for the
    ME405 board. 
    '''
    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer, t_1, t_2):
         ''' 
         @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
         @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
         @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
         @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
         @param timer A pyb.Timer object to use for PWM generation on
         IN1_pin and IN2_pin. 
         '''
         ## Create the motor sleep pin
         self.pin_nSLEEP = pyb.Pin (nSLEEP_pin, pyb.Pin.OUT_PP)
         
         ## Set the pin low initially to disable the motor
         self.pin_nSLEEP.low()
         
         ## Define pin object 1
         self.pin_IN1 = pyb.Pin(IN1_pin) 
         
         ## Define pin object 2
         self.pin_IN2 = pyb.Pin(IN2_pin) 
         
         ## Create a timer object for pin 1
         self.tch1 = timer.channel(t_1, pyb.Timer.PWM,pin = self.pin_IN1)
         
         ## Create a timer object for pin 2
         self.tch2 = timer.channel(t_2, pyb.Timer.PWM,pin = self.pin_IN2)
    
    def enable (self):
         ''' 
         @brief Sets the sleep pin high
         '''
         self.pin_nSLEEP.high()
    
    def disable (self):
         ''' 
         @brief Sets the sleep pin low
         '''
         self.pin_nSLEEP.low()
    
    def set_duty (self, duty):
         ''' 
         @brief This method sets the duty cycle to be sent
         to the motor to the given level. Positive values
         cause effort in one direction, negative values
         in the opposite direction.
         @param duty A signed integer holding the duty
         cycle of the PWM signal sent to the motor 
         '''
         if duty > 0:
             if duty > 100 :
                 duty = 100
             self.tch1.pulse_width_percent(0)
             self.tch2.pulse_width_percent(duty)
         else:
             if duty < -100:
                 duty = -100
             self.tch2.pulse_width_percent(0)
             self.tch1.pulse_width_percent(abs(duty))

