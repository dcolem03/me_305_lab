'''
@file main.py

@brief main file that runs on startup on the nucleo

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

from Encoder import Encoder
from Nucleo_Task import TaskNucleo
import pyb

# Create an encoder object
my_encoder = Encoder(3, pyb.Pin.cpu.A6, 1, pyb.Pin.cpu.A7, 2, 0, 0xFFFF)

#Create an encoder task
Nucleo_Task = TaskNucleo(my_encoder, 1, 200000, False)

# Continuously run the encoder task
while True:
    Nucleo_Task.run()
        
        
