# -*- coding: utf-8 -*-
'''
@file Elevator_FSM_main.py

This file creates two elevator task objects and runs them
'''
from Elevator_FSM import Button, MotorDriver, TaskElevator

# This code would be in your main project most likely (main.py)
        
# Creating objects to pass into task constructors
button_1_a = Button('PB6')
button_2_a = Button('PB7')
first_floor_a = Button('PB8')
second_floor_a = Button('PB9')
Motor_a = MotorDriver()

button_1_b = Button('PC6')
button_2_b = Button('PC7')
first_floor_b = Button('PC8')
second_floor_b = Button('PC9')
Motor_b = MotorDriver()

# Creating two task objects using the button and motor objects above
taska = TaskElevator(0.1, button_1_a, button_2_a, first_floor_a, second_floor_a, Motor_a)
taskb = TaskElevator(0.1, button_1_b, button_2_b, first_floor_b, second_floor_b, Motor_b)

# Run the tasks in sequence over and over again
for N in range(10000000):
    taska.run()
    taskb.run()
