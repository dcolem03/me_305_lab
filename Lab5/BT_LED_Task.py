'''
@file BT_LED_Task.py

@brief Bluetooth LED task as a finite state machine

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
from pyb import UART
from Bluetooth import Bluetooth

class TaskBTLED:
    '''
    @brief      Bluetooth LED task
    
    @details    The Bluetooth LED task interfaces with a smartphone application
                that controls an LED on a nucleo board. The user is able to turn
                the LED on and off ass wells as being able to make it blink at a 
                given frequency
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waiting for user input
    S1_WAIT_FOR_INP    = 1
    
    ## Changing LED state
    S2_CHANGE_LED_STATE      = 2

    def __init__(self, my_Bluetooth, taskNum, interval, dbg):
        '''
        @brief Creates a Blutooth LED task object.
        @param my_Bluetooth A Bluetooth object
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        ## The object controled by this task
        self.my_Bluetooth = my_Bluetooth
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Boolean to wether the LED is on or off
        self.LED_On = 0
        
        ## Steady State of LED, 0 is off, 1 is on , 2 is blinking
        self.LED_SS = 0
        
        ## Variable for the user input frequency of LED blinking
        self.frequency = 0
        
        self.input = ''
        
        # Check to run dbg code
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_INP)
            
            elif(self.state == self.S1_WAIT_FOR_INP):
                self.printTrace()
                # Run State 1 Code
                
                # if there are any charactrers waiting in UART, transistion to S2
                if self.my_Bluetooth.readAny():
                    self.input = self.my_Bluetooth.readLine().decode()
                    self.transitionTo(self.S2_CHANGE_LED_STATE)
                # else check to see if the LED needs to be turned on or off
                else:
                    # Off button pressed, LED should be off
                    if self.LED_SS == 0:
                        self.my_Bluetooth.off()
                    # On button pressed, LED shoud be on
                    elif self.LED_SS == 1:
                        self.my_Bluetooth.on()
                    # else LED should be blinking, change the state
                    else:
                        if self.LED_On == 1:
                            self.my_Bluetooth.on()
                            self.LED_On = 0
                        else:
                            self.my_Bluetooth.off()
                            self.LED_On = 1
                            
            elif(self.state == self.S2_CHANGE_LED_STATE):
                self.printTrace()
                # Run State 2 Code
                
                # If the user pressed the on button, set the variable so the LED is on
                if self.input == 'on':
                    self.LED_SS = 1
                    self.my_Bluetooth.writeLine('The LED is now on')
                # If the user pressed the off button, set the variable so the LED is off
                elif self.input == 'off':
                    self.LED_SS = 0
                    self.my_Bluetooth.writeLine('The LED is now off')
                # Else see if the users input is valid and set the frequency
                else:
                    try:
                        self.frequency = int(self.input)
                        # If the frquency is valid, change the interval to match the user's desired frequency
                        if self.frequency > 0:
                            self.interval = int(1000000*1/self.frequency)
                            self.LED_SS = 2
                            temp_str = 'The LED is now blinking at ' + str(self.frequency) + ' Hz'
                            self.my_Bluetooth.writeLine(temp_str)
                        # Else print an error message
                        else:
                            self.frequency = 0
                            self.my_Bluetooth.writeLine('Error, invalid input. Please enter a positive integer')
                    except:
                        self.my_Bluetooth.writeLine('Error, invalid input. Please enter a positive integer')
                self.transitionTo(self.S1_WAIT_FOR_INP) 
                
            else:
                # error handling
                print('error, re-initializing')
                self.transitionTo(self.S0_INIT)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)

