'''
@file Bluetooth.py

@brief This file contains the bluetooth class 

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
from pyb import UART


class Bluetooth:
    '''
    @brief      A class that contains the methods to initialize and update a bluetooth module
    @details    This class creates a bluetooth object and contains functions to read 
                inputs from the bluetooth module as well as write lines to the bluetooth 
                module. It also contains functions to turn a connected LED on and off.
    '''
    
    def __init__(self, UART_Port, UART_Baudrate, LED_Pin ):
        '''
        @brief Initializes the bluetooth object
        @param UART_Port The port that you want the UART created with
        @param UART_Baudrate The desired baudrate of the UART
        @param LED_Pin The pyb.pin object connected to the LED you wish to control
        
        '''
        self.uart = UART(UART_Port, UART_Baudrate)
        
        self.pin = pyb.Pin (LED_Pin, pyb.Pin.OUT_PP)
        

    def writeLine(self, input):
        '''
        @brief Sends a string to be displayed on the input device
        @param input desired text to be displayed    
        '''
        self.uart.write(input)
        
    
    def readLine(self):
        '''
        @brief    Reads the characters in the UART
        @return   Characters in the UART
        '''  
        return self.uart.readline()
       
    def readAny(self):
        '''
        @brief    Checks if any characters are waiting in the UART
        @return   A boolean value to indicate if characters are waiting
        '''  
        return self.uart.any()  
 
    def on(self):
        '''
        @brief Sets the slected pin to high
        '''  
        self.pin.high()
    
    def off(self):
        '''
        @brief Sets the selected pin to low         
        '''
        self.pin.low()
        
