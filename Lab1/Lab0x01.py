"""
@file Lab0x01.py

@brief      This file contains the function fib as well as a statement to run fib in main.
@details    The function fib promts the user to input an index of the fibonacci sequence
            that they would like to know the value of, then that value is calculated.

@author Daniel Coleman
@date   9/28/20
"""


def fib ():
    '''
    @brief      A function that determines the nth value of the fibonnacci sequence.
    @details    This function will prompt the user for an index of the fibinnacci 
                sequence that they would like to know the value of. Once the user 
                inputs a value, the function first checks if the user would like
                to exit, then it checks that the user has input a valid integer,
                and finally it calculates the value of the fibonacci sequence.
                Once this is completed, the function call itself so the user can
                ask for the value of another index.
    '''
    # user input that is hopefully either a valid integer or 'exit'
    my_inp = input('Please enter an index for the Fibonacci number. If you would like to stop, type "exit": ')
    
    # if the user types exit, then the looping function will stop
    if my_inp == "exit":
        return
    
    # using 'try' allows my_inp to be converted to an integer without throwing
    # an error if it is not an integer
    try:
        my_inp = int(my_inp) 
        
        # Checking to see if the integer is positive
        if my_inp > 0:
            print ('Calculating Fibonacci number at '
               'index n = {:}.'.format(my_inp))
            print('.')
            
            # Main body of code that calculates the value of the fibonacci sequence
            # at index my_inp. Variables are constantly overwritten rather than 
            # stored in a data structure to save compute time.
            i = 0
            old, new = 0, 1
            for i in range(0,my_inp):
                old, new = new, old+new
                i += 1
            
            # Printing the answer
            print(old)
            
        else:
            print('Error, please enter a valid integer')
    
    # the 'except' prints a soft error message and lets the user try again
    except ValueError:
        print('Error, please enter a valid integer')
    
    # Function calls itself so that the user is able to try multiple indices        
    fib()
    
if __name__ == '__main__':
    fib()
