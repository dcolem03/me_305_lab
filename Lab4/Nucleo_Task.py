'''
@file Nucleo_Task.py

@brief Nucleo task as a finite state machine

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
from pyb import UART
from Encoder import Encoder

class TaskNucleo:
    '''
    @brief      Nucleo task.
    
    @details    An object of this class waits to recieve a user input and when
                it is recieved, if it is correct, it starts collecting data. After
                either 10 s or if a user interupt is recived, data collection and
                transmission is stopped and the object goes back to waiting for
                an input.
    
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CHAR    = 1
    
    ## Waiting for response state
    S2_RUN_ENCODER      = 2

    def __init__(self, my_Encoder, taskNum, interval, dbg):
        '''
        Creates a Nucleo task object.
        @param my_Encoder An encoder object
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        ## The object controled by this task
        self.my_Encoder = my_Encoder
    
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        ## Definies the initial time when data collection starts
        self.t_0 = 0
        
        ## defines time since data collection has started
        self.t_diff = 0
        
        # Check to run dbg code
        if self.dbg:
            print('Created user interface task')

    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.transitionTo(self.S1_WAIT_FOR_CHAR)
            
            elif(self.state == self.S1_WAIT_FOR_CHAR):
                self.printTrace()
                # Run State 1 Code
                
                # Wait for an input character
                if self.ser.readchar() == 71:
                    # Set initial values and transition states
                    self.t_0 = self.curr_time
                    self.t_diff = 0
                    self.transitionTo(self.S2_RUN_ENCODER)
                        
            
            elif(self.state == self.S2_RUN_ENCODER):
                self.printTrace()
                # Run State 2 Code
                
                # Check for a user interupt
                if self.ser.readchar() == 83:
                    # Transition states
                    self.transitionTo(self.S1_WAIT_FOR_CHAR) 
                # Check if 10 s of data has been collected
                if self.t_diff <= 10000000:
                    # Update encoder and print current state
                    self.my_Encoder.update()
                    print('{:}, {:}'.format(round(self.t_diff/1000000, 1),round(self.my_Encoder.get_Position()/(7*4*50)*360,1)))
                    self.t_diff = self.curr_time - self.t_0
                else:
                    # After 10 s, transition to state 1
                    self.transitionTo(self.S1_WAIT_FOR_CHAR)          
            else:
                # error handling
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)
