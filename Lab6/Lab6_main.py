'''
@file Lab6_main.py

@brief The main script to run on the nucleo on startup

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
from MotorDriver import MotorDriver
from EncoderDriver import EncoderDriver
from ClosedLoop import ClosedLoop
from ControllerTask import TaskController

pin_nSLEEP = pyb.Pin.cpu.A15;

pin_IN1 = pyb.Pin.cpu.B0;
pin_IN2 = pyb.Pin.cpu.B1;

pin_OUT1 = pyb.Pin.cpu.C6;
pin_OUT2 = pyb.Pin.cpu.C7;    
        
# Create the timer object used for PWM generation
moetim = pyb.Timer(3,freq=20000)
enctim = pyb.Timer(8)
            
# Create a motor object passing in the pins and timer
moe1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, moetim, 3, 4)
moe1.set_duty(0)
enc1 = EncoderDriver(pin_OUT1, pin_OUT2, enctim, 1, 2)
    

CL = ClosedLoop(1000)
    
task = TaskController(moe1, enc1, CL, 1, 5000, False)

# Set the duty cycle
while True:
    task.run()
