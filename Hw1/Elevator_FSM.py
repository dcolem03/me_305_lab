'''
@file Elevator_FSM.py

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary two story
elevator without doors.

The user has a button to move the elevator to floor 1 or 2.

There is also a button indicating which floor the elevator is currently stopped
at.
'''

from random import choice
import time

class TaskElevator:
    '''
    @brief      A finite state machine to control an elevator.
    @details    This class implements a finite state machine to control the
                operation of an elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0    
    
    ## Constant defining State 1
    S1_MOVING_DOWN  = 1    
    
    ## Constant defining State 2
    S2_MOVING_UP = 2    
    
    ## Constant defining State 3
    S3_STOPPED_FLOOR_1      = 3    
    
    ## Constant defining State 4
    S4_STOPPED_FLOOR_2     = 4
    
    def __init__(self, interval, button_1, button_2, first_floor, second_floor, Motor):
        '''
        @brief              Creates a TaskElevator object.
        @param interval     A double value representing the time between each loop of the FSM
        @param button_1     An object from class Button representing the button to select floor 1
        @param button_2     An object from class Button representing the button to select floor 2
        @param first_floor  An object from class Button representing the existence of the elevator at floor 1
        @param second_floor An object from class Button representing the existence of the elevator at floor 2
        @param Motor        An object from class Motor representing the raising/lowering mechanicsm for the elevator
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## The button object used for
        self.button_1 = button_1
        
        ## The button object used for
        self.button_2 = button_2
        
        ## The button object used for
        self.first_floor = first_floor
        
        ## The button object used for
        self.second_floor = second_floor
        
        ## The motor object 
        self.Motor = Motor
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ##
        self.interval = interval
        
        ## The "timestamp" for when the task should run next
        self.next_time = time.time() + self.interval 
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = time.time()
        if self.curr_time > self.next_time:
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Down()
                print(str(self.runs) + ': State 0  ' + str(time.time()))
            
            elif(self.state == self.S1_MOVING_DOWN):
                # Run State 1 Code
                if(self.first_floor.getButtonState()):
                    self.transitionTo(self.S3_STOPPED_FLOOR_1)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 1  ' + str(time.time()))
            
            elif(self.state == self.S2_MOVING_UP):
                # Run State 2 Code
                if(self.second_floor.getButtonState()):
                    self.transitionTo(self.S4_STOPPED_FLOOR_2)
                    self.Motor.Stop()
                print(str(self.runs) + ': State 2  ' + str(time.time()))
            
            elif(self.state == self.S3_STOPPED_FLOOR_1):
                # Transition to state 1 if the left limit switch is active
                if(self.button_2.getButtonState()):
                    self.transitionTo(self.S2_MOVING_UP)
                    self.Motor.Up()
                print(str(self.runs) + ': State 3  ' + str(time.time()))
            
            elif(self.state == self.S4_STOPPED_FLOOR_2):
                # Run State 4 Code
                if(self.button_1.getButtonState()):
                    self.transitionTo(self.S1_MOVING_DOWN)
                    self.Motor.Down()
                print(str(self.runs) + ': State 4  ' + str(time.time()))
            
            else:
                # Invalid state code (error handling)
                print ('error')
                pass
            
            self.runs +=1
            
            # specifying the next time the task will run
            self.next_time += self.interval
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        

class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                a user within an elevator, or by the elevator once it has reached 
                a floor. As of right now this class is implemented using "pseudo-hardware". 
                That is, we are not working with real hardware IO yet, this is all pretend.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to move the elevator
                up and down
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Up(self):
        '''
        @brief Moves the motor Up
        '''
        print('Elevator going Up')
    
    def Down(self):
        '''
        @brief Moves the motor Down
        '''
        print('Elevator going Down')
    
    def Stop(self):
        '''
        @brief Stops the motor
        '''
        print('Elevator Stopped')



























