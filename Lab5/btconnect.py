
import pyb
from pyb import UART

uart = UART(3, 9600)

pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

while True:
    if uart.any() != 0:  
        val = int(uart.readline())              
        if val == 0:                  
            print(val,' turns it OFF')                
            pinA5.low()
            uart.write('LED off')            
        elif val == 1:                
            print(val,' turns it ON')
            uart.write('LED on') 
            pinA5.high()
