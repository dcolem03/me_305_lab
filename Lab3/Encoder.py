'''
@file Encoder.py

@brief This File contains the encoder class which can be combined with its Encoder_Task finite state machine to provide information on the encoder position

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb


class Encoder:
    '''
    @brief      A class that contains the methods to initialize and update a motor encoder
    '''
    
    def __init__(self, Timer_Number, Pin_1, Channel_Pin_1, Pin_2, Channel_Pin_2, Prescaler, Period):
        '''
        @brief                  Initializes the encoder object
        @param Timer_Number     An integer reflecting the desired timer number 
        @param Pin_1            A pin object for encoder channel 1
        @param Channel_Pin_1    An integer reflecting the channel for pin 1
        @param Pin_2            A pin object for encoder channel 2
        @param Channel_Pin_2    An integer reflecting the channel for pin 2
        @param Prescaler        An integer value to be used as the timer prescaler
        @param Period           An intefer value to be used as the timer period
        '''
        self.Timer_Number = Timer_Number
        
        self.Pin_1 = Pin_1
        
        self.Channel_Pin_1 = Channel_Pin_1
        
        self.Pin_2 = Pin_2
        
        self.Channel_Pin_2 = Channel_Pin_2
        
        self.Prescaler = Prescaler
        
        self.Period = Period
        
        self.tim = pyb.Timer(self.Timer_Number)
        
        self.tim.init(prescaler = self.Prescaler, period = self.Period)
        
        self.tim.channel(self.Channel_Pin_1, pin = self.Pin_1, mode = pyb.Timer.ENC_AB)
        
        self.tim.channel(self.Channel_Pin_2, pin = self.Pin_2, mode = pyb.Timer.ENC_AB)
        
        self.Current_State = 0
        
        self.Position = 0
        
        self.Delta = 0
        
        #self.Previous_State = 0

    def update(self):
        '''
        @brief      Updates values within the encoder class that define its position
        @details    This method updates the delta and current_state values to reflect the 
                    current state of the encoder. Then it decides if the encoder has 
                    overflowed, underflowed, or neither, and updates the absolute
                    position of the encoder since the last restart
        '''
        self.Delta = self.tim.counter() - self.Current_State
        self.Current_State = self.tim.counter()
        if abs(self.get_Delta()) < self.Period/2:
            self.Position += self.Delta
        elif self.Delta>0:
            self.Position += self.Period-self.Delta
        else:
            self.Position += self.Period+self.Delta
        
    
    def get_Position(self):
        '''
        @brief    Returns the current position of the encoder     
        @return   An integer representing the current position of the encoder
        '''  
        return self.Position
        
    def set_Position(self, New_Position):
        '''
        @brief                  Sets the current position to a desired value 
        @param New_Position     An integer value representing the desired position of the encoder
        '''
        self.Position = New_Position
        
    def get_Delta(self):
        '''
        @brief   Returns the delta value between the current state and previous state
        @return  An integer value representing the difference between the two states
        '''    
        return self.Delta


