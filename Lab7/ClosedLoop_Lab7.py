'''
@file ClosedLoop_Lab7.py

@brief This file contains the Closed Loop class for calculating the input to a mechanical system

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''


class ClosedLoop:
    '''
    @brief      A class that contains the methods to initialize and update a closed loop object
    '''
    
    def __init__(self):
        '''
        @brief Initializes the Closed Loop object
        '''
        self.Kp = 0
        self.Kp_p = self.Kp/3.3
        
    def run (self, Om_curr, Om_ref, Th_curr, Th_ref):
        '''
            @brief Runs the closed loop P controller algorithm        
            @return The desired motor duty cycle
            @param Om_curr Current motor speed
            @param Om_ref Desired motor speed
            @param Th_curr Current motor position
            @param Th_ref Desired motor position
        ''' 
        return self.Kp_p*((Om_ref - Om_curr) + (5/3)*(Th_ref - Th_curr))
    
    def get_Kp (self):
        '''
            @brief Returns the stored value for Kp
            @return The stored value for Kp
        ''' 
        return self.Kp
    
    def set_Kp (self, Kp):
        '''
            @brief Sets the value for Kp and Kp_p
            @param Kp Desired value for Kp
        ''' 
        self.Kp = Kp
        self.Kp_p = self.Kp/3.3
