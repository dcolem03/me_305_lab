
'''
@file Encoder_Main.py

@brief This file creates an encoder object, an encoder task object, and a ui task object and runs the task simultaneously.

@author Daniel Coleman
'''
from Encoder import Encoder
from Encoder_Task import TaskEncoder
from UI_Task import TaskUI
import pyb

Interval = 10

my_encoder = Encoder(3, pyb.Pin.cpu.A6, 1, pyb.Pin.cpu.A7, 2, 0, 0xFFFF)
encoderTask = TaskEncoder(my_encoder, 1, Interval, dbg=False)
uiTask = TaskUI(2, Interval, dbg=False)


# Run the tasks simultaneously
for N in range(1000000):
    encoderTask.run()
    uiTask.run()