import pyb
from Bluetooth import Bluetooth
from BT_LED_Task import TaskBTLED
import utime



UART_Port = 3
UART_Baudrate = 9600
LED_Pin = pyb.Pin.cpu.A5

BT = Bluetooth(UART_Port, UART_Baudrate, LED_Pin)

interval = 100000

BT_Task = TaskBTLED(BT, 1, interval, False)

while True:                           
    BT_Task.run()
