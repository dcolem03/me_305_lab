'''
@file BTmain.py

@brief main file that creates and runs a Blutooth Task object

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import pyb
from Bluetooth import Bluetooth
from BT_LED_Task import TaskBTLED
import utime


# Create a Blutooth Object that connects to the LED pin
UART_Port = 3
UART_Baudrate = 9600
LED_Pin = pyb.Pin.cpu.A5

BT = Bluetooth(UART_Port, UART_Baudrate, LED_Pin)

# Create a BT_Task Object Using the Bluetooth Object
interval = 100000

BT_Task = TaskBTLED(BT, 1, interval, False)


# Continuously run BT_Task
while True:                           
    BT_Task.run()
