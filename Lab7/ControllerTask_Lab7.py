'''
@file ControllerTask_Lab7.py

@brief A file containing the Controller Task with an integrated back end UI

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
from pyb import UART



class TaskController:
    '''
    @brief Task Controller waits for an input to be recieved from the UI and then runs the motor for 5 seconds.
    
    @details Task Controller first waits for in a value to be received from the UART, then set that as the value for Kp in the Closed Loop Object.
    From there it transitions to collecting data wherein the encoder is upated, the closed loop algorithm is run, and a new duty cycle is sent to 
    the motor. At the same time, data is being sent back to the computer side UI via serial.
    '''

    ## Initialization state
    S0_INIT             = 0
    
    ## Waiting for user input
    S1_WAIT_FOR_INP    = 1
    
    ## Changing LED state
    S2_GET_MOTOR_RESP    = 2

    def __init__(self, motor, encoder, CL, taskNum, interval, dbg):
        '''
        @brief Creates a Blutooth LED task object.
        @param motor
        @param encoder
        @param CL
        @param taskNum A number to identify the task
        @param interval An integer number of microseconds between desired runs of the task
        @param dbg A boolean indicating whether the task should print a trace or not
        '''
        
        ## Define local motor object
        self.motor = motor
        
        ## Define local encoder object
        self.encoder = encoder
        
        ## Define local closed loop object
        self.CL = CL
        
        ## The number of the task
        self.taskNum = taskNum
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval)
        
        ## Flag to print debug messages or supress them
        self.dbg = dbg
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Serial port
        self.ser = UART(2)
        
        ## Local variable to mark when the task starts running
        self.start = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start, self.interval)

        ## Local variable to mark when the task starts running
        self.start_time = 0
        
        ## Local variable for the current velocity of the motor
        self.velocity = 0
        
        ## Local variable for the current value of Kp
        self.Kp = self.CL.get_Kp()
        
        ## Local time list to correspond to velocity and position lists
        self.T_ref = []
        
        ## Reference motor velocity
        self.Om_ref = []
        
        ## Reference motor position
        self.Th_ref = []
        
        # Open the reference file
        ref = open('reference.csv');
        
        # Read data indefinitely. Loop will break out when the file is done.
        while True:
            # Read a line of data. It should be in the format 't,v,x\n' but when the
            # file runs out of lines the lines will return as empty strings, i.e. ''
            line = ref.readline()
            
            # If the line is empty, there are no more rows so exit the loop
            if line == '':
                break
            
            # If the line is not empty, strip special characters, split on commas, and
            # then append each value to its list.
            else:
                (t,v,x) = line.strip().split(',');
                self.T_ref.append(float(t))
                self.Om_ref.append(float(v))
                self.Th_ref.append(float(x))
            # Only collect 1 in 100 data points
            i = 0
            while i < 99:
                line = ref.readline()
                i = i+1
                
        ## Loacal variable for the index in the reference lists
        self.index = 0
        
        ## Variable for evaluating system performance
        self.J = 0

        # Check to run dbg code
        if self.dbg:
            print('Created Controller task')

    def run(self):
        '''
        @brief Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            
            if(self.state == self.S0_INIT):
                self.printTrace()
                # Run State 0 Code
                self.motor.set_duty(0)
                self.transitionTo(self.S1_WAIT_FOR_INP)
            
            elif(self.state == self.S1_WAIT_FOR_INP):
                self.printTrace()
                # Run State 1 Code
                
                # Wait for an input from the UART
                if self.ser.any():
                    # Ensure that a valid entry is recieved
                    try:
                        # Set the value for Kp and initialize motor, encoder and closed loop
                        self.Kp = float(self.ser.readline())
                        self.CL.set_Kp(self.Kp)
                        self.start_time = utime.ticks_us()
                        self.motor.enable()
                        self.encoder.update()
                        # Transition to next state
                        self.transitionTo(self.S2_GET_MOTOR_RESP)
                    # error handling
                    except:
                        print("not an integer")    
                            
            elif(self.state == self.S2_GET_MOTOR_RESP):
                self.printTrace()
                # Run State 2 Code
                
                # While there are still values in the reference lists
                try:
                    # Ensure that the correct reference values are being used
                    while utime.ticks_diff(self.curr_time, self.start_time)/1000000 > self.T_ref[self.index]:
                        self.index = self.index +1
                    # Update encoder and get velocity and position
                    self.encoder.update()
                    self.velocity = (self.encoder.get_Delta()/4000)/(self.interval/(1000000*60))
                    self.position = self.encoder.get_Position()*360/4000
                    self.J = self.J + (self.velocity - self.Om_ref[self.index])**2 + (self.position - self.Th_ref[self.index])**2
                    # Print the system state for the front end UI to read
                    print('{:}, {:}, {:}, {:}'.format(round(utime.ticks_diff(self.curr_time, self.start_time)/1000000, 3),round(self.velocity,1),round(self.position,1),round(self.J/self.runs,1)))
                    # Set the motor duty cycle according to it's current state
                    self.motor.set_duty(self.CL.run(self.velocity, self.Om_ref[self.index], self.position, self.Th_ref[self.index]))
                # Once the time limit has been reached
                except:
                    # Reset all variables
                    self.motor.disable()
                    self.motor.set_duty(0)
                    self.encoder.set_Position(0)
                    self.index = 0
                    self.J = 0
                    self.transitionTo(self.S1_WAIT_FOR_INP)
                
            else:
                # error handling
                print('error, re-initializing')
                self.transitionTo(self.S0_INIT)
                pass
            
            self.runs += 1
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)

    def transitionTo(self, newState):
        '''
        @brief Updates the state variable
        '''
        self.state = newState

    def printTrace(self):
        '''
        @brief Prints a debug statement with a detailed trace message if the debug variable is set
        '''
        if self.dbg:
            str = 'T{:}/S{:}/R{:}\t@ {:9.0f}:'.format(self.taskNum, self.state, self.runs, utime.ticks_diff(self.curr_time,self.start_time))
            print(str)

