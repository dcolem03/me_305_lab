'''
@file Lab4_UART.py

@brief This script acts as the Spyder user interface for lab 4

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import serial
import matplotlib.pyplot as plt
import time
import csv

# Create a serial port for nucleo communication
ser = serial.Serial(port='COM3',baudrate=115273,timeout=0.5)

# run twice
for n in range(1):
    
    # Create Time and Position lists
    Time = []
    Position = []
    
    # Ask for user input
    inv = input('Please enter "G" to start collecting Encoder Data: ')
    
    #Check if user input is valid
    if inv == 'G':
        print('Thank you, data is now being recorded.')
        # Send uesr input to nucleo
        ser.write(str(inv).encode('ascii'))
        
        # Check for user stopping data collection
        inv2 = input('Type "S" if you would like to end recording early, otherwise, press Enter:')
        ser.write(str(inv2).encode('ascii'))
        
        # Wait for data to be collected and sent
        time.sleep(10)
        
        # Organize data into the proper data structures
        while ser.in_waiting > 0:
            line_string = ser.readline().decode('ascii')
            line_list = line_string.strip().split(',')
                                                  
            Time.append(float(line_list[0]))
            Position.append(float(line_list[1]))
        
        # Plot data
        plt.plot(Time,Position)
        plt.xlabel('Time [s]')
        plt.ylabel('Motor Position [deg]')
        plt.show()  
    
        # Create csv file
        with open('Lab_4_Data.csv', 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(Time)
            writer.writerow(Position)

    
    else:
        print('Invalid input, please try again')
    

    
    
ser.close()
