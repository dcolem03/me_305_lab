'''
@file EncoderDriver.py

@brief This File contains the encoder class 

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import utime
import pyb
import math

class EncoderDriver:
    '''
    @brief      A class that contains the methods to initialize and update a motor encoder
    '''
    
    def __init__(self, OUT1_pin, OUT2_pin, timer, t_1, t_2):
        '''
        @brief                  Initializes the encoder object
        @param Timer_Number     An integer reflecting the desired timer number 
        @param Pin_1            A pin object for encoder channel 1
        @param Channel_Pin_1    An integer reflecting the channel for pin 1
        @param Pin_2            A pin object for encoder channel 2
        @param Channel_Pin_2    An integer reflecting the channel for pin 2
        @param Prescaler        An integer value to be used as the timer prescaler
        @param Period           An intefer value to be used as the timer period
        '''
        
        ## Define pin 1 locally
        self.Pin_1 = OUT1_pin
        
        ## Define pin 2 locally
        self.Pin_2 = OUT2_pin
        
        ## Set the period for the timer
        self.Period = 0xFFFF
        
        ## Create the timer object
        self.tim = timer
        
        ## Initialize the timer
        self.tim.init(prescaler = 0, period = self.Period)
        
        ## Create timer channel 1
        self.tim.channel(t_1, pin = self.Pin_1, mode = pyb.Timer.ENC_AB)
        
        ## Create timer channel 2
        self.tim.channel(t_2, pin = self.Pin_2, mode = pyb.Timer.ENC_AB)
        
        ## Set the encoder state to 0
        self.Current_State = 0
        
        ## Set the encoder position to 0
        self.Position = 0
        
        ## Set the encoder delta to 0
        self.Delta = 0

    def update(self):
        '''
        @brief      Updates values within the encoder class that define its position
        @details    This method updates the delta and current_state values to reflect the 
                    current state of the encoder. Then it decides if the encoder has 
                    overflowed, underflowed, or neither, and updates the absolute
                    position of the encoder since the last restart
        '''
        self.Delta = self.Current_State - self.tim.counter()
        self.Current_State = self.tim.counter()
        
        if abs(self.Delta) > self.Period/2:
            if self.Delta > 0:
                self.Delta = -(self.Period-self.Delta)

            else:
                self.Delta = (self.Period+self.Delta)

               
        self.Position += self.Delta
        
    
    def get_Position(self):
        '''
        @brief    Returns the current position of the encoder     
        @return   An integer representing the current position of the encoder
        '''  
        return self.Position
        
    def set_Position(self, New_Position):
        '''
        @brief                  Sets the current position to a desired value 
        @param New_Position     An integer value representing the desired position of the encoder
        '''
        self.Position = New_Position
        
    def get_Delta(self):
        '''
        @brief   Returns the delta value between the current state and previous state
        @return  An integer value representing the difference between the two states
        '''    
        return self.Delta


