'''
@file UIFrontEnd_Lab7.py

@brief This script acts as the Spyder user interface for lab 6

@author Daniel Coleman

@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

import serial
import matplotlib.pyplot as plt
import time

# Create a serial port for nucleo communication
ser1 = serial.Serial(port='COM3',baudrate=115273,timeout=0.5)

# run
for n in range(1):
    
    # Create Time, Velocity and Position lists
    Time = []
    Velocity = []
    Position = []
    J = []
    
    # Create Reference Time, Velocity and Position lists
    reftime = []
    refvelocity = []
    refposition = []
    
    # Open the reference csv file
    ref = open('reference.csv');
        
    # Read the reference file and parse out the colums into seperate time position
    # and velocity lists
    while True:

        line = ref.readline()

        if line == '':
            break

        else:
            (t,v,x) = line.strip().split(',');
            reftime.append(float(t))
            refvelocity.append(float(v))
            refposition.append(float(x))
    
    # Ask for user input
    inv = input('Please enter a positive number for gain Kp: ')
    
    # Check if user input is valid, prevent invalid outputs
    try:
        test = float(inv)
        print('Thank you, data is now being recorded.')
        # Send user input to nucleo
        ser1.flushInput()
        ser1.write(str(inv).encode('ascii'))
        # Wait for data to be collected and sent
        time.sleep(15)

        # Organize data into the proper data structures
        while ser1.in_waiting > 0:
            line_string = ser1.readline().decode('ascii')
            line_list = line_string.strip().split(',')
                                                  
            Time.append(float(line_list[0]))
            Velocity.append(float(line_list[1]))
            Position.append(float(line_list[2]))
            J.append(float(line_list[3]))
            
        plt.figure(1)
        
        # Plot data
        plt.subplot(2,1,1)
        plt.plot(Time, Velocity, label = 'Actual Response')
        plt.plot(reftime, refvelocity, label = 'Reference Response')
        plt.xlabel('Time [s]')
        plt.ylabel('Motor Speed [RPM]')
        plt.legend(loc = 'best')
        plt.title('Motor Response with J = ' + str(J[len(J)-1]))

        
        plt.subplot(2,1,2)
        plt.plot(Time,Position,label = 'Actual Response')
        plt.plot(reftime, refposition, label = 'Reference Response')
        plt.xlabel('Time [s]')
        plt.ylabel('Motor Position [deg]')
        plt.legend(loc = 'best')
        
        
        plt.show()
          
    except:
        print('Error, please try again')
ser1.close()
    
   
    

    